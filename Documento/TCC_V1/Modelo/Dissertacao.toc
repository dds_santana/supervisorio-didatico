\babel@toc {brazil}{}
\babel@toc {english}{}
\babel@toc {brazil}{}
\contentsline {chapter}{Lista de Figuras}{xi}{chapter*.2}%
\contentsline {chapter}{Lista de Tabelas}{xiii}{chapter*.3}%
\contentsline {chapter}{Lista de Quadros}{xiv}{section*.4}%
\contentsline {xchapter}{}{xiv}{section*.4}%
\contentsline {chapter}{\numberline {1}Introdução}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Contextualização e Justificativa}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Objetivos}{3}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Objetivos Gerais}{3}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Objetivos Específicos}{3}{subsection.1.2.2}%
\contentsline {section}{\numberline {1.3}Estrutura do Trabalho}{3}{section.1.3}%
\contentsline {chapter}{\numberline {2}Conceitos Básicos}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Modelagem de Processos}{5}{section.2.1}%
\contentsline {section}{\numberline {2.2}Linearização de sistemas}{6}{section.2.2}%
\contentsline {section}{\numberline {2.3}Controladores}{6}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}PID}{7}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}LQR}{8}{subsection.2.3.2}%
\contentsline {section}{\numberline {2.4}SCADA}{9}{section.2.4}%
\contentsline {section}{\numberline {2.5}Comunicação Serial}{11}{section.2.5}%
\contentsline {section}{\numberline {2.6}Qt em Python}{13}{section.2.6}%
\contentsline {chapter}{\numberline {3}Desenvolvimento do Sistema Supervisório}{17}{chapter.3}%
\contentsline {section}{\numberline {3.1}Requisitos do Sistema}{17}{section.3.1}%
\contentsline {section}{\numberline {3.2}Seleção das Tecnologias}{17}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Seleção das bibliotecas}{18}{subsection.3.2.1}%
\contentsline {section}{\numberline {3.3}A interface gráfica}{20}{section.3.3}%
\contentsline {section}{\numberline {3.4}\emph {Dataset Config}}{21}{section.3.4}%
\contentsline {section}{\numberline {3.5}\emph {PlotManager}}{24}{section.3.5}%
\contentsline {section}{\numberline {3.6}\emph {MainPlotArea}}{25}{section.3.6}%
\contentsline {section}{\numberline {3.7}\emph {SCADADialog}}{26}{section.3.7}%
\contentsline {section}{\numberline {3.8}Salvamento Automático de Séries}{29}{section.3.8}%
\contentsline {chapter}{\numberline {4}Validação do Supervisório Didático}{31}{chapter.4}%
\contentsline {section}{\numberline {4.1}Metodologia}{31}{section.4.1}%
\contentsline {section}{\numberline {4.2}Apresentação do Sistema}{32}{section.4.2}%
\contentsline {section}{\numberline {4.3}Comportamento esperado}{34}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Linearização do Sistema}{35}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Sintonia do PI}{36}{subsection.4.3.2}%
\contentsline {subsection}{\numberline {4.3.3}Sintonia do LQR}{36}{subsection.4.3.3}%
\contentsline {section}{\numberline {4.4}Configuração do supervisório didático}{37}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Controlador PI}{37}{subsection.4.4.1}%
\contentsline {subsection}{\numberline {4.4.2}LQR}{38}{subsection.4.4.2}%
\contentsline {section}{\numberline {4.5}Validação dos dados}{40}{section.4.5}%
\contentsline {chapter}{\numberline {5}Conclusão}{45}{chapter.5}%
\contentsline {section}{\numberline {5.1}Trabalhos futuros}{46}{section.5.1}%
\contentsline {chapter}{Refer\^encias}{47}{chapter*.27}%
\contentsline {chapter}{Anexos e Apêndices}{49}{chapter*.27}%
\contentsline {chapter}{\numberline {A}Tutorial de utilização do supervisório didático}{51}{appendix.A}%
\contentsline {section}{\numberline {A.1}Apresentação}{51}{section.A.1}%
\contentsline {section}{\numberline {A.2}Primeira utilização}{51}{section.A.2}%
\contentsline {section}{\numberline {A.3}Iniciando o programa}{52}{section.A.3}%
\contentsline {section}{\numberline {A.4}Importando séries estáticas no programa}{52}{section.A.4}%
\contentsline {subsection}{\numberline {A.4.1}Função de Transferência}{52}{subsection.A.4.1}%
\contentsline {subsection}{\numberline {A.4.2}Entrada por arquivo}{53}{subsection.A.4.2}%
\contentsline {subsection}{\numberline {A.4.3}Serial}{54}{subsection.A.4.3}%
\contentsline {subsection}{\numberline {A.4.4}Script Python}{54}{subsection.A.4.4}%
\contentsline {section}{\numberline {A.5}Monitoramento em tempo real}{55}{section.A.5}%
\contentsline {section}{\numberline {A.6}Salvando e editando séries de dados}{56}{section.A.6}%
\contentsline {section}{\numberline {A.7}Plotando séries}{56}{section.A.7}%
\contentsline {section}{\numberline {A.8}Editando e exportando gráficos}{56}{section.A.8}%
\contentsline {section}{\numberline {A.9}Deletando séries}{57}{section.A.9}%
\contentsline {chapter}{\numberline {B}Códigos}{59}{appendix.B}%
\contentsline {chapter}{\numberline {C}Códigos}{61}{appendix.C}%
\contentsline {section}{\numberline {C.1}Código do arduino para Controle de Tanque com Área Variável}{61}{section.C.1}%
